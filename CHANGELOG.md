# CHANGELOG

## 1.1.1 (24.01.2023)

### FIX

- Set MIT as default licence

## 1.1.0 (09.01.2023)

### FEATURES

- Add security.txt

## 1.0.1 (03.01.2023)

### FIX

- Typos

## 1.0.0 (02.01.2023)

### FEATURES

- MVP initial release
