export interface Answer {
  label: string;
  imgSrc: string;
  text: string;
  next?: number;
  solutions: string[];
  last: boolean;
  warning?: any;
}

export interface Decision {
  question: string;
  answers: Answer[];
  prev: number;
  info?: string;
}

export interface WarningMessage {
  title: string;
  text: string;
}

export type Decisions = Array<Decision>;
