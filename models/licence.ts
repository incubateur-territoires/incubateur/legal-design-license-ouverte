export enum Types {
  PERMISSIVE = 'Permissive',
  COPYLEFT = 'Copyleft',
}
export const types = Object.values(Types);

export enum Origins {
  COMMUMAUTAIRE = 'Communautaire',
  INSTITUTIONNELLE = 'Institutionnelle',
  INDUSTRIELLE = 'Industrielle',
}
export const origins = Object.values(Origins);

export enum Purposes {
  LOGICIELS = 'Logiciels',
  BDD = 'Bases de données',
  AUTRE = 'Autres contenus',
}

export const purposes = Object.values(Purposes);

export interface Licence {
  slug: string;
  name: string;
  summary: string;
  pros: string[];
  cons: string[];
  type: string[];
  origin: string[];
  purpose: string[];
  projet_link: string;
  projet_name: string;
  projet_img: string;
}

export type Licences = Array<Licence>;
