export interface Image {
  directus_files_id: string;
}
export interface Projet {
  slug: string;
  name: string;
  description: string;
  images: Image[];
  competence: string;
  license: number;
}
export interface Licence {
  name: string;
  id: number;
}
