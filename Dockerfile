FROM node:20 AS build
ADD . /app
WORKDIR /app
RUN npm ci
RUN npm run build

FROM gcr.io/distroless/nodejs:18
WORKDIR /app
COPY --from=build /app/.output /app/.output

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000

CMD [".output/server/index.mjs"]
