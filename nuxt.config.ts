// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    baseURL: process.env.NUXT_BASE_URL ?? '/',
  },
  css: ['@gouvfr/dsfr/dist/dsfr.min.css', '@gouvfr/dsfr/dist/utility/icons/icons-system/icons-system.min.css', '@/assets/scss/style.scss'],
  modules: [
    'nuxt-directus',
    process.env.MATOMO_URL && process.env.MATOMO_SITE_ID
      ? ['nuxt-matomo', { matomoUrl: process.env.MATOMO_URL, siteId: process.env.MATOMO_SITE_ID }]
      : null,
  ],
  directus: {
    url: process.env.DIRECTUS_URL || 'localhost',
    token: process.env.DIRECTUS_TOKEN || 'dummytoken',
  },
  runtimeConfig: {
    public: {
      matomoSiteId: '',
      matomoUrl: '',
    },
  },
});
