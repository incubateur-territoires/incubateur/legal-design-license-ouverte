import { defineNuxtPlugin } from '#app';
import VueMatomo from 'vue-matomo';

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig();

  if (config.public.matomoSiteId && config.public.matomoUrl) {
    nuxtApp.vueApp.use(VueMatomo, {
      host: config.public.matomoUrl,
      siteId: config.public.matomoSiteId,
      router: nuxtApp.$router,
      enableLinkTracking: true,
      requireConsent: false,
      trackInitialView: true,
      disableCookies: true,
      requireCookieConsent: false,
    });
  }
});
