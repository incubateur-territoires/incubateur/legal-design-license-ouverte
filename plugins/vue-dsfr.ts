import VueDsfr from '@gouvminint/vue-dsfr';
import { RiInformationLine, RiCheckLine } from 'oh-vue-icons/icons/ri/index';
import { MdTimer, MdKeyboarddoublearrowrightSharp } from 'oh-vue-icons/icons/md/index';
import { LaExternalLinkAltSolid } from 'oh-vue-icons/icons/la/index';
import { defineNuxtPlugin } from '#app';

const icons = [RiInformationLine, RiCheckLine, LaExternalLinkAltSolid, MdTimer, MdKeyboarddoublearrowrightSharp];

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueDsfr, { icons });
});
